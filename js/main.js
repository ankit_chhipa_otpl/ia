$( document ).ready(function() {
    

    new Splide( '.whatwedosplide', {
        arrows: false,
        grid: {
            rows: 3,
            cols: 5,
            gap : {
                row: '10px',
                col: '10px',
            }
        },
        breakpoints: {
            600: {
                grid: {
                    rows: 3,
                    cols: 3,
                    gap : {
                        row: '10px',
                        col: '10px',
                    }
                },
            }
        }
    } ).mount( window.splide.Extensions );


    new Splide( '.verticals-splide', {
        type   : 'loop',
        perPage: 5,
        perMove: 1,
        pagination: false,
        gap: '10px',
        arrowPath: 'M3986.914,6703.434l12.531,10.922-12.531,11.3" transform="translate(-3972.579 -6695.057)" fill="none" stroke="#fff" stroke-width="1"',
        breakpoints: {
            600: {
                perPage: 1,
            }
        }
    } ).mount();

    var secondarySlider = new Splide( '.profile-splide', {
        type   : 'loop',
        perPage: 5,
        perMove: 1,
        gap: '10px',
        pagination: false,
        isNavigation: true,
        cover: true,
        fixedWidth: '100px',
        height: '96px',
        arrowPath: 'M3986.914,6703.434l12.531,10.922-12.531,11.3" transform="translate(-3972.579 -6695.057)" fill="none" stroke="#fff" stroke-width="1"',
        breakpoints: {
            600: {
                perPage: 3,
                fixedWidth: '80px',
                height: '76px',
            }
        }
    } ).mount();

    var primarySlider = new Splide( '#primary-slider', {
        type: 'slide',
        gap: '10px',
        cover: true,
        pagination: false,
        arrows :false,
	});

    primarySlider.sync( secondarySlider ).mount();

    new Splide( '.main-banner-slider', {
        type   : 'loop',
        pagination: false
    } ).mount();


    function animateValue(obj, start = 0, end = null, duration = 3000) {
        if (obj) {
    
            // save starting text for later (and as a fallback text if JS not running and/or google)
            var textStarting = obj.innerHTML;
    
            // remove non-numeric from starting text if not specified
            end = end || parseInt(textStarting.replace(/\D/g, ""));
    
            var range = end - start;
    
            // no timer shorter than 50ms (not really visible any way)
            var minTimer = 100;
    
            // calc step time to show all interediate values
            var stepTime = Math.abs(Math.floor(duration / range));
    
            // never go below minTimer
            stepTime = Math.max(stepTime, minTimer);
    
            // get current time and calculate desired end time
            var startTime = new Date().getTime();
            var endTime = startTime + duration;
            var timer;
    
            function run() {
                var now = new Date().getTime();
                var remaining = Math.max((endTime - now) / duration, 0);
                var value = Math.round(end - (remaining * range));
                // replace numeric digits only in the original string
                obj.innerHTML = textStarting.replace(/([0-9]+)/g, value);
                if (value == end) {
                    clearInterval(timer);
                }
            }
    
            timer = setInterval(run, stepTime);
            run();
        }
    }
    
    animateValue(document.getElementById('startups'));
    animateValue(document.getElementById('locations'));
    animateValue(document.getElementById('founders'));


});